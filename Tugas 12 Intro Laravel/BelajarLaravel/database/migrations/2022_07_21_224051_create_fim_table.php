<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFimTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fim', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul');
            $table->string('ringkasan');
            $table->integer('tahun');
            $table->string('poster');
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')
      ->references('id')->on('genre')
      ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fim');
    }
}
