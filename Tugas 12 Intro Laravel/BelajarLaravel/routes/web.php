<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@utama');
Route::get('/register','AuthController@register');
Route::post('/welcome','AuthController@send');

Route::get('/data-tables',function(){
    return view('datatable');
});

Route::get('/table',function(){
    return view('table');
});

// CRUD CAST

// menuju form input cast
Route::get('/cast/create','castController@create');

// menyimpan data cast
Route::post('/cast','CastController@store');

// Tampil data cast
Route::get('/cast','castController@index');
// Tampil detail cast
Route::get('/cast/{id}','castController@show');

// update Cast
Route::get('/cast/{id}/edit','castController@edit');
Route::put('/cast/{id}','castController@update');

// delete Cast
Route::delete('/cast/{id}','castController@destroy');

// CRUD Film
Route::resource('film','filmController');
