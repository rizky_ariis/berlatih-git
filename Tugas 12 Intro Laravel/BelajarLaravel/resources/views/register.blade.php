@extends('master')
@section('judul')
  Form
@endsection
@section('content')

<form action="welcome" method="POST">
    @csrf
  <h1>Buat Account Baru!</h1>
  <h3>Sign Up Form</h3>

  <label>First name :</label><br /><br />
  <input type="text" name="first" /><br /><br />
  <label>Last name :</label><br /><br />
  <input type="text" name="last" /><br /><br />
  <label>Gender :</label><br /><br />
  <input type="radio" name="gender" />Male<br />
  <input type="radio" name="gender" />Female<br /><br />
  <label>Nationality :</label><br /><br />
  <select name="nationality">
    <option value="1">Indonesia</option>
    <option value="2">England</option>
    <option value="3">United State</option>
    <option value="4">Netherland</option></select
  ><br /><br />
  <label>Language Spoken :</label><br /><br />
  <input type="checkbox" name="language" />Bahasa Indonesia<br />
  <input type="checkbox" name="language" />English<br />
  <input type="checkbox" name="language" />Other<br /><br />
  <label>Bio :</label><br /><br />
  <textarea name="bio" id="" cols="30" rows="10"></textarea> <br /><br />
  <input type="Submit" value="Sign Up" />
</form>
@endsection


  
