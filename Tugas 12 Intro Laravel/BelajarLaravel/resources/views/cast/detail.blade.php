@extends('master')

@section('judul')
  Halaman Detail Cast
@endsection
@section('content')

<div class="card">
    <div class="card-body">
        <h3>Nama : {{$cast->nama}}</h3>
        <h4>Umur : {{$cast->umur}}</h4>
        <h4>Bio  : {{$cast->bio}}</h4>
    </div>
  </div>
  <a href="/cast" class="btn btn-secondary btn-sm mt-3">Kembali</a>
@endsection