@extends('master')
{{-- @push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush --}}
@section('judul')
  Halaman Data Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah Cast</a>
<table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            {{-- <th scope="col">Umur</th>
            <th scope="col">Bio</th> --}}
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key =>$item)
        <tr>
            <td>{{$key + 1 }}</td>
            <td>{{$item->nama}}</td>
            {{-- <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td> --}}
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn sm">detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn sm">Edit</a>
                    @method('delete')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
          </tr>
      @empty
          <tr>
              <td>Tidak ada data</td>
            </tr>
            @endforelse
        </tbody>
    </table>
 
    @endsection