@extends('master')
@section('judul')
  Halaman Film
@endsection
@section('content')

    <a href="/film/create" class="btn btn-primary btn-sm my-3">Tambah Film</a>
    <div class="row">
        @forelse ($film as $item)
        
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img src={{asset('image/'.$item->poster)}} class="height:50px widht:100px" alt="...">
                    <div class="card-body">
                      <h5 >{{$item->judul}}</h5>
                      <p class="card-text">{{$item->ringkasan}}</p>
                      <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail Film</a>
                    </div>
                  </div>
            </div>
        
        @empty
            <h1>Tidak ada film</h1>
        @endforelse
    </div>
@endsection