@extends('master')
@section('judul')
  Halaman Detail Film
@endsection
@section('content')

<a href="/film" class="btn btn-primary btn-sm my-3">Kembali</a>
<div class="row">
        <div class="col">
            <div class="card" style="width: 18rem;">
                <img src={{asset('image/'.$film->poster)}} class="height:50px widht:100px" alt="...">
                <div class="card-body">
                  <h5 >{{$film->judul}}</h5>
                  <p class="card-text">{{$film->tahun}}</p>
                  {{-- <p class="card-text">{{$film->}}</p> --}}
                  <p class="card-text">{{$film->ringkasan}}</p>
                </div>
              </div>
        </div>
</div>

@endsection