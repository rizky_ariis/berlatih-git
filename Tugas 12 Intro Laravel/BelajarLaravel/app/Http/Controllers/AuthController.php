<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    
    public function send(Request $request){
        
        $firstName = $request['first'];
        $lastName = $request['last'];

        return view('success',compact('firstName','lastName'));
    }
}
