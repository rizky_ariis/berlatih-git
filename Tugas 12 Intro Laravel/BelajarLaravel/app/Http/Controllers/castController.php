<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio' => 'required',
            'publish_at' => 'nullable|date',
        ],
        [
            'nama.reruired'=>'Nama Tidak Boleh Kosong',
            'umur.required'=>'Umur tidak boleh kosong',
            'umur.integer'=>'Inputan harus berupa angka',
            'bio.required'=>'Bio tidak boleh kosong'
        ]
    );

    DB::table('cast')->insert(
        [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]
    );

    return redirect('/cast');

    }


    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index',compact('cast'));
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail',compact('cast'));//'cast' dari $cast
    }
    
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.update',compact('cast'));//'cast' dari $cast
    }

    public function update(request $request,$id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|integer',
            'bio' => 'required',
            'publish_at' => 'nullable|date',
        ],
        [
            'nama.reruired'=>'Nama Tidak Boleh Kosong',
            'umur.required'=>'Umur tidak boleh kosong',
            'umur.integer'=>'Inputan harus berupa angka',
            'bio.required'=>'Bio tidak boleh kosong'
        ]
    );

            DB::table('cast')
              ->where('id', $id)
              ->update([
                'nama' => $request['nama'],
                'umur'=>$request['umur'],
                'bio'=>$request['bio']
            ]
        );

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }


}
